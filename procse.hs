import System.IO
import Graphics.UI.Gtk hiding (disconnect)
import Graphics.UI.Gtk hiding (get)
import Graphics.UI.Gtk.Glade
import Graphics.UI.Gtk.ModelView as Model
import Control.Concurrent.MVar
import CPi.Lib
import CPi.Parser
import CPi.Semantics
import CPi.ODE
import CPi.Plot
import CPi.Logic
import CPi.Matlab

import qualified Control.Monad.State.Strict as Strict
import Control.Monad.State.Strict 
import qualified Data.List as L


pros :: [Definition] -> [Process]
pros [] = []
pros ((SpeciesDef _ _ _):xs) = pros xs 
pros ((ProcessDef _ proc):xs) = proc : (pros xs)
