

\documentclass[10pt,twocolumn]{article}


\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{natbib}
\usepackage{url}
\usepackage[pdftex]{graphicx}
\usepackage{fullpage} 
\usepackage{wrapfig}
\usepackage{multirow}

\title{Minf Project Phase 1 Report\\
 Cpi-IDE: A GUI for Continuous-pi calculus}

\author{Andrew McCrae\\
	S0835826}
%\date{} 

\begin{document}
\maketitle


\section{Introduction}

\subsection{Purpose}

The aim of this project is to create a graphical user interface tool that allow for interactive development and exploration of models written in the language Continuous $\pi$-calculus. Continuous $\pi$-calculus (Cpi) is a process algebra, used for modelling biochemical molecular systems by expressing their behaviour and interactions over time. The tool has been proposed as a, more capable, graphical interface alternative to an existing prototype command line tool currently in development for the language.

\subsection{Outline}
This report discusses the progress made on the project in phase 1 of its implementation. Section 2 briefly recounts the background and motivation for the proposed tool. Section 3 gives an overview of the design and implementation proposed in the previous project planning report \citep{MPP}. Section  4 describes the progress made on the implementation during this phase. This is followed by an evaluation of this progress in section 5. In section 6 a series of different goals for future work are discussed The final section 7 of the report shows an intended plan for further implementation in phase 2 of the project.
%\pagebreak
\section{Background}
This section aims to describe the related background and express motivation for the project. Given is a brief description of the language Continuous $\pi$-calculus (Cpi) and the existing tool for the language, Cpi Workbench.


\subsection{Continuous $\pi$-calculus}
Continuous $\pi$-calculus (Cpi) created by Marek Kwiatkowski and Ian Stark is a process algebra used to express and analyse biochemical systems\citep{kwiatkowski/stark:continuous-pi}. Cpi model definitions have a dedicated syntax that allow for the expression of system components, their interactions and behaviour. The language is based on the $\pi$-calculus\citep{Milner} and focuses on bimolecular systems. The language uses two distinct forms components that can be defined as: Species and Processes. Species definitions represent individual molecules and their possible behaviour for transition within the system. Process definitions describe the overall population of the species within a system and denote the concentration of each with a real number. For all points in time a process defines a complete model of a system, arising from the transitions of species. The system definitions can be converted into a series of Ordinary Differential Equations (ODE’s) which when solved give a simulation of the component concentrations over time represented in the form of vector spaces.
Below is a simple example of a Cpi definition in which a populations of 3 species, X,Y and Z interacting within one process Pi. Molecules of the first two species X, which has a starting concentration of 2, and Y, which has a starting concentration of 1, combine in the process Pi which results in both being converted into a Z molecule. Z has a starting concentration of 0 but as the process continues this rises to 2 until the population of Y molecules runs out, resulting in no further changes in transition for any of the species. This system would be defined using Cpi with the following code:\\

\begin{figure*}[!]
\centering
\includegraphics[width = 0.75\textwidth]{XYZ.png}
\caption{Plot of Example Systems}
\end{figure*}

\pagebreak

\begin{verbatim}
species X(a) = a.Z();
species Y(b) = b.Z();
species Z(c) = c.0;

process Pi = [2] X(a) || [1] Y(b) || 
[0] Z(c) : {a-b@1};
\end{verbatim}
Figure 1 shows the concentrations of the system population through their interaction in process Pi over time.\\
This is an example of a small simple system in CPi but the language can be used to model large and highly complex models. Cpi has been used to model several biochemical systems such as Enzyme catalysis, the KaiC circadian clock of the cyanobacterium Synechococcus elongatus and the MAPK cascade model.
Cpi is still currently under research at the University of Edinburgh and there is a prototype command line tool in development called Continuous pi Workbench CpiWB.

\subsection{Cpi Workbench} 
Cpi Workbench, developed by Chris Banks, captures the key analysis features needed to explore models written in the language\citep{CPIWB}. The tool has features including: 
\begin{center}
\begin{figure*}[t]
\centering
\includegraphics[width = 0.75\textwidth]{cpiwb1.png}
\caption{Cpi Workbench}
\end{figure*}
\end{center}



\begin{itemize}
\item Viewing the component definitions.
\item Calculating and solving the ODE's.
\item Plotting these solutions to show the outcome of models.
\item Exporting these representations in various file types.
\item Model checking to give user's the ability to query the models on specific details.
\end{itemize} 


%\begin{wrapfigure}{r}{0.55\textwidth}
%\includegraphics[width=0.55\textwidth]{cpiwb1.png}
%\caption{Cpi Workbench}
%\end{wrapfigure}

However the tool is based entirely in the command line and is not as accessible as other tools for similar languages, several of which are discussed in the project planning report\citep{MPP}.The tool has some short comings regarding accessibility in that the user must interact through purely text based entry entry in the command line, edit the model definitions in separate text editing applications. Whilst the graphed outputs of the tool can also be exported they must be viewed in a different window from the tool and the parameters and model definitions cannot be dynamically altered. Figure 2 shows an example of the Mapk cascade model in Cpi-WB.
\\\\


After analysis of the Cpi workbench and research a series of similar tools, of which again more details can be found in the planning report, a design was drafted for the application.

%\pagebreak
\section{Specification and Design}


\begin{figure*}[t]
\centering
\begin{tabular}{ | p{0.2\textwidth} | p{0.52\textwidth} | p{0.2\textwidth}  | }
\hline 
    Category & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column    
    Key & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
    Additional  \\\hline
%%%%%%%%%%%%%%%%%%%%%%%%%%column
%%%%%%%%%%%%%%%%%%%%%%%%%%row
    Viewing and editing of models & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column    
    Loading existing Cpi model files within the application.\newline
    Editing of Cpi model source code within the tool. \newline
    Saving new or altered Cpi models to file. & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
    Syntax highlighting of the code. \\\hline
%%%%%%%%%%%%%%%%%%%%%%%%%%row
    Static analysis & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
    Compiling the Cpi model within the tool.\newline
    Representing the parsed model specification.\newline
    Expressing the calculated ODE's.& 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
    Affinity networks \\\hline
%%%%%%%%%%%%%%%%%%%%%%%%%%row
    Dynamic analysis & 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
    Solve the ODE's \newline
    Plot the solution of the ODE's in graph form\newline
    Represent the data in tabular format.\newline
     Export simulation results& 
%%%%%%%%%%%%%%%%%%%%%%%%%%column
     Model checking  \\\hline
    
\end{tabular}
\caption{Features specified in planning report}
\end{figure*}
The original design proposed for the project specified the tool as a plug-in for the Eclipse Interactive Development Environment. During the planning stage of the project the decision was made to create the tool as a stand alone application\citep{MPP}. After researching similar tools that were both Eclipse plug-ins and stand alone applications, it was decided that a stand alone tool for Cpi models would provide a more focused set of functionality for the user to explore Cpi models. 
\\Thus the planning report went on to give the design specification regarding the functionality and layout of components within the stand alone Cpi-IDE tool. This section will briefly recap the design given for the tool broken into two categories, the intended functionalities of the tool and the layout of these components that provide the functionality within the application.


\subsection{Functionality}
The application is intended to provide a more capable tool for utilizing Cpi models then the Cpi Workbench. To do this Cpi-IDE should include most if not all of the features provided by the Workbench, as well as a further set of capabilities to allow for a more accessible exploration of CPi models. In the planning report a set of intended functionalities were outlined for the new tool. Some of these are considered more desirable then others and each were of different priority in the order of implementation, with some features classed as Key and others Additional. Specified Key features were considered to be top priority, being essential functionalities of the tool. These should definitely be implemented for the tool to successfully achieve its overall aim. Additional features are desired features that would be beneficial extra functionalities of the tool, but implementation of these should only be pursued if the essential features are completed. The table in figure 3 shows the both the key and additional goals from the planning specification. They are split into the same three areas of the functionality as in the previous report, viewing and editing of Cpi models, static analysis of the model definition without simulation and the dynamic analysis of simulated models showing the outcomes of the experiment.
\begin{center}

\begin{figure*}
\centering
\includegraphics[width=0.75\textwidth]{cpiide.png}
\caption{Mock up of application}
\end{figure*}
\end{center}

%\begin{wrapfigure}{r}{0.52\textwidth}
%\includegraphics[width=0.5\textwidth]{cpiide.png}
%\caption{Mock up of application}
%\end{wrapfigure}

\subsection{Layout}
During the planning stage of the project a mock-up of the application was created using Visual Basic to aide in the design and layout specification for the tool. Figure 4 shows a screen-shot of the mock-up to demonstrate the intended layout of the components. 
It can be seen that the design interface consists of a text editing interface on the left side of the tool that allows the user to create and edit models. Then on the right hand side of the tool various forms of analysis are given, in the screen-shot the graphical plot of the solutions is shown. Each of the different forms of analysis are contained within an individual tab of the right hand of the application.

%\pagebreak
\section{Implementation}
Below is a description of current implementation of the Cpi-IDE application. 
\begin{figure*}[ht!]
\centering
\fbox{{\includegraphics[width=0.75\textwidth]{proto.png}}}
\caption{Cpi IDE}
\end{figure*}
\subsection{Architecture}

The application is written in the programming language Haskell and utilizing the Gtk2hs tool-kit\citep{GTK2HS} to provide a graphical interface. Gtk2hs is the Haskell binding to the GTK+ cross platform graphics tool-kit. GTK+ is popular for development of GUI's for many applications\citep{GTK}. It uses a combination of various components called widgets which are designed for specific user interaction purposes such as text input or tool-bar buttons etc. To create the user interface the Glade design tool is used to specify the layout, properties and interaction of these GTK+ widgets\citep{GLADE}. The details of this specification are saved to an XML file to be loaded by Gtk2hs in when the application is run. 
This gives the application an interface with the back-end Haskell providing the functionalities.
The tool integrates the parser used in the CpiWB to read in the model definitions.
To keep track of file locations and the parsed definitions the program uses MVar Haskell mutable variables to maintain the state of this information within the tool.

\subsection{Design}

The current prototype of the application has adhered closely to the design laid out in the planning report.
Figure 5 shows a model simulation using the prototype tool.



In the image we can see that the model definitions are contained in an editor found on the left hand side of the tool. On the right hand side of the figure the tool is displaying a graphical plot of the model simulation from the given set of time-scale parameters. This shows how the design has stayed very close to that of the planned design seen in figure 4.

\subsection{Implementation}
The tool is as mentioned above composed of a collection of GTK+ widgets. These widgets individually or combined with others provide the realization for the proposed functionality. 

\subsubsection{Viewing and Editing models}

\begin{itemize}
\item Text editor:
\\*The text editor is realized by a textView widget. This maintains a buffer of the text to hold the Cpi source code. In this field text can be written into or existing models may be loaded in from file. This can be seen in figure 5 as the large text entry area on the left half of the tool.
    
\item File opening:
\\* The tool uses a fileChooserDialog widget to specify the location of the Cpi file intended to be opened. This allows for navigation through the file system and once Cpi file is selected the open button will read the contents of the file into the text buffer contained in the main editing text view. An example of this can be see in figure 6.
\begin{figure*}
\centering
\fbox{\includegraphics[width=0.75\textwidth]{cpiopen.png}}
\caption{File Open Dialogue}
\end{figure*}

\item File saving:
\\*Initially the saving of Cpi models was not intended to be implemented until the next semester. After implementing the file opening capabilities it became apparent that this could quickly be included as well utilising another fileChooserDialog widget to implement the "save as" function. The tool also provides the straight "save" functionality of saving a file that has been loaded or saved previously in the same location, without the use of a fileChooserDialog to dictate the file location again. To achieve this the file location and name are stored in a MVar within the program to allow for easy saving of models as the process of editing occurs.

\end{itemize} 

\subsubsection{Static Analysis}

\begin{itemize}

\item Parsing the model:
\\*To compile the model definition from the file in the editor the tool integrates the existing Cpi parser from the CpiWb. Instead of being passed a file as in the Workbench the contents of the text buffer is passed to the parser. If there is an error the application will return the relevant message to the user in the field where the parsed definitions are returned. If the model definition is successfully parsed the application will display the corresponding definitions of the components.

\item Component Definitions:
\\*Once parsed the components of the model are displayed within a textView widget to express the components name and their starting concentration and their interactions with other components. This currently uses the same text format as that used in the Workbench. Discussed later in the report is the aim to alter this to a better representation through the utilization of a treeView widget to provide a clearer and more detailed view of the definitions within a table.

\item ODE's:
\\From the model definitions the tool calculates a series of ordinary differential equations that represent the changes in component concentrations over time. The formulas for these are displayed to the user again using a textView widget with their name and the corresponding equation. The ODE representation also is intended to be altered in the next phase to better represent the definitions in another treeView widget.
\end{itemize} 

\subsubsection{Dynamic Analysis}

\begin{itemize}

\item{Solving the ODE's}
\\To solve the series of ODE's calculated from the model the tool utilizes the same built-in solver as the CPiWB. The Workbench has the capability to use external solvers in certain large and complex model cases that cannot be handled by this solver. Currently Cpi-IDE does not have this ability as it does not need to handle such complex models at this time. This is intended to be included at a later date to ensure the tool can handle any Cpi model.

\item{Plot the Solved ODE's}
\\ There is a collection of labeled textEntry widgets to take in the necessary time set parameters ensures they are suitable constraints. Using these parameters the application plots the resulting solutions in a graph in a GTK+ drawingArea widget that has a pre-defined size. The tool can also output the graph to file in various file formats for exportation outside of the tool.

\end{itemize}



\section{Evaluation}
This section provides an evaluation on the progress of the project the phase 1 by using goals set in the planning report regarding the functionality of the application. This section also discusses several other factors regarding the project such as the scope of the original goals and challenges found in implementation.

\subsection{Functionality}
To evaluate the progress made so far on the tool we can look at the goals set in the planning report for this stage of implementation\citep{MPP}. In the planning report the project had a series of deliverables were set for end of phase 1 these consisted of :
\begin{itemize}
\item Opening existing Cpi models from file.
\item Parsing the Cpi model definitions.
\item Providing static analysis of the model.
\item Calculating the ODE's generated by the model.
\item Plotting the solved ODE's by a given series of parameters.
\end{itemize}
As is shown in section 4 currently each of these goals have been successfully implemented. The tool now has a graphical interface that allows for both editing of models in the application and analysis of the model simulations within the same interface. 
The tool now also includes some further capabilities as well as these. With several of the goals for this semester being implemented faster then expected. During this phase of the project the opportunity arose to complete some of the goals intended for next semester. The capabilities intended for the next phase that have already been implemented are:
\begin{itemize}
\item Editing Cpi models within the tool.
\item Saving models to file.
\item Exporting graphs to file.
\end{itemize}
Having met all of the goals for this point in the implementation and also a series of further goals intended for the next phase of the project it appears that the project implementation is ahead of its target for the end of this phase. Shown in figure 3 above there are 10 key functionalities that the tool was intended to include. At this point according to the planning report only 5 of the 10 were intended to be completed. Cpi-IDE currently has 9 out of the 10, with only tabular representation of the simulation results not included in the tool, with this also previously intended to be completed in phase 2. This indicates that the tool implementation is exceeding the original expectations and is on track to successfully achieve almost all the goals set for the end of the project

\subsection{Scope}
The additional progress made on the project so far has shown that, whilst the implementation is exceeding the aims set out in the planning report, this is almost certainly due to a fairly conservative implementation plan. Software development projects often experiences poor estimation of effort and time required, it is more common that projects aims are unrealistic in being too ambitious and projects often fall behind schedule\citep{FFSE}. In this case the  poor estimation of the time required for the implementation means that the scope was not ambitious enough and in reality a larger set of goals are achievable in the time frame given. One of the main issues with projected schedules for software development is that the estimation only occurs at the beginning of the project. This project has seen a similar error in estimation in that the time-frame for goals were not fully understood until implementation began. The scope of the project plan should be extended due to this, with a much richer array of features and functions possible for the end tool, examples of these are discussed in section 6. 
\\*During the implementation of the project and composing this report it has become apparent that during the planning stage of the project the scope of measures for evaluation and testing of the tool could have been extended as well. A further series of evaluation criteria could have been constructed with criteria such as user testing to evaluate the tool regarding usability as well as the functionality provided by the tool. Due to this in the Workplan section for the next phase of the project below discusses several additional functionality inclusions to extend the tool to a much more usable and useful tool then the current version. For the next phase it may also be of value to gain at least some if not a series of user interaction with the tool to provide a better iterative process for providing focus on important features.
\\* Another issue with the scope of the project is that so far none of the functionalities, save for file editing, have been extensions that allow for further analysis then the Workbench. Whilst the graphical interface is more accessible and the model can be edited easier, the tool does not currently provide any novel additional forms of representation or analysis and mostly encompasses the same features as the Workbench. From the work in this phase the tool only needs basic model checking to provide all the functionality of the Workbench. In the next phase more focus should be put on providing a richer set of analysis or representation that shows that Cpi-IDE is a more capable tool then the Workbench

\subsection{Challenges}
Whilst the tool is now capable of providing a full simulation of a model created from scratch or loaded from file there are some minor challenges that were faced during the implementation. 
\\One challenge faced was the translation of data from the Cpi parser into suitable GTK+ display widgets. Currently a series of textView widgets are used to display various definitions. These are not represented as they were intended and further work is needed to better display these.
\\* Another difficulty arose from Haskell's natural aversion to maintaining state. The implementation of GUI's generally lends itself to object orientated languages due to the ease in maintaining mutable states and handling events. However it is more then possible to achieve a Graphical interface with functional languages in Haskell and various methods can be set to keep variables and manage state within a Haskell application. In the project the application needed to keep the certain information such as the file name and the parsed definition of the model. To maintain these required the use of the Haskell concurrent library and the MVar (Mutable Variable).


%\pagebreak


\section{Goals}
In the planning report a series of goals were set for the end of first semester in second year of the project. The overall aim set for the next semester, was a fully functioning tool that matches the specification. This term fully functioning can be seen as meeting all key goals in functionality set in the planning report. Since almost all key goals have been achieved it would seem that at least one if not all of the additional goals may be feasible.
The only remaining key functionality from the planning specification is the table representation of the model simulations. An implementation of this has already begun and this should be the highest priority for next feature to be added and should be finished relatively quickly.
\\*Below is a description of the goals still required from the planning report and several potential additional features that could possibly be worked on in phase 2. These have been separated in to two categories: Definite goals, which are feasible desired extensions to the tools functionality whose implementations are predicted to be achievable within the given time-frame. The second category is a series of potential extensions to the tools whose scope makes them less feasible then the definite goals. These would be highly sought after to enrich the functionality of the tool if the opportunity arose to at least partially if not fully implement, but due to their complexity should be approached once the more realistic goals have been completed.


\subsection{Definite Goals}

\begin{itemize}
\item Tabular Representation of Simulation Results:
\\*One of the key goals set out in the planning report was to represent the results of simulated models in table format to allow for the analysis and presentation of results as well as those given in graph format. Implementation on this has begun and the representation of data in treeView widget within its own tab is the intended goal to realize this functionality. The data is already generated from the solved ODE's and all the is needed is a suitable representation.

\item Interface Improvements:
\\*Discussed in section 5.4 the tool design is quite simple and basic. Before the tool can be assessed by a user group several improvements to the design can be made to make the functionality and features available more accessible. The menu bar at the top of the tool currently contains file handling operations but should also include exportation and processing capabilities as well as basic text editing  short-cuts. Though the tool has a series of buttons that provide access to each of the functionalities these are not clearly labelled and could be grouped once further features are available into a richer tool bar widget. One of the issues raised at the previous user group interactions was the counter intuitive positions of buttons, whilst this has been changed with guidance from the comments it would be  beneficial to take this into account and receive feedback from the users.

\item Tutorial for Tool
\\*In some similar tools features such as "wizards" and tutorials show the users how to operate the application in a step by step process. For testing with the user group the implementation of such a feature may help the accessibility of the tool and allow for a easier operation of Cpi-IDE. The tutorial will work through a basic model such as that in section 2 and introduce the user to the syntax of Cpi and the definitions of models.


\item Further Experiment Exportation:
\\*Similar tools that were researched in the during the planning stage of the project had the functionality to allow users to not only save model definitions but export entire experiments, including data resulting from simulations. When looking for extensions to the tools functionality this seemed like a beneficial feature to include. The aim of this would be to export the simulated data so that it can be shared and used in other applications, such as Matlab or spreadsheet applications, in formats for use outside of the tool, such as CSV. This would require some decision regarding formatting but would be relatively straightforward as the process would be able to utilize functionality similar to that used in file saving  by the tool at the moment.

\item Formula Based Model Checking:
\\*There are two possible implementations of the model checking that could be used to provide the functionality. The first of these is by using a simple text input field, that will allow the user to enter formula and receive answers, in a similar fashion to that provided by the Workbench. This would be relatively simple to implement and quick to integrate. Providing built in model checking functionality was proposed in the planning report as a possible means of extending the application of the tool. To do this will involve integrating the logic engine from the Cpi Workbench. Initially this functionality was still in development for the command line tool when the project began, but is now an integral feature in the analysis of models within the workbench. Once capable of model checking the tool will include all the  features possessed by the Workbench. Due to the ease of previous integrations of functionality from the Workbench this should be relatively straightforward to implement. 

\end{itemize}


\subsection{Potential extensions}

\begin{itemize}

\item Query Builder Model Checking:
\\*The second possibility to implement model checking is to create a query builder tool for the user to interact with an input form to specify the outcomes they would like to investigate. This is slightly more difficult as to create a query builder that successfully covers all possible combinations of input would be fairly complex. Compared to other features provided in the tool this would require a significant amount of time to design. Other features were adapted from existing functionality in the workbench or have been based on features in similar tools, which has provided a clear design for the Cpi-IDE features resemble. In this case to ensure that the implementation of the query builder provides the intended accessibility the feature would need to be significant time to be comprehensively designed before implementation. During the user group when asked about which of these options would be preferable, the point was raised that the query builder is designed to make input of question easier. However the tools use relies on an understanding and ability to write in Cpi, based on the text input used in the workbench it was found that if the users could comfortably write their model in Cpi they would understand enough to write their queries in the formulaic style.     to these factors I believe that the best strategy would be to first implement the text input 

\item Phase Plots
\\*Another possible avenue of graphical representation is to explore the model simulation outcomes. One proposed method is to express the simulations in a phase plots of the solved ODE's in the system. These are 2 or 3 dimensional plots with each dimensions denoting a variable, such as time or species concentration and plots the results in the system space given by these dimensions. This would again be useful for demonstrating experiment results for reports and presentations. This would require a much more straight forward implementation method then the possible graphical model definition representations as there is not a lot of design needed. There are similar tools with examples of how to represent ODE's in phase plots and the tool would simply need to calculate the graphed outputs as it can already handle the graph plotting.


\item Syntax Highlighting
\\* During the research some of the similar tools that were researched provided syntax highlighting that altered the colour/font of the source code definitions of models. This is intended to make the writing of the source code easier as it gives may alert the users to any structure or syntax errors in the code.  This could be realized using the GTKsourceview package within the text editor field. This would require a specification of the Cpi syntax and some decisions on font changes used to highlight components. There with examples of this package within Haskell applications found in other tools such as Leksah. This goal  is in this category not because of it's complexity but due to its fairly low priority compared to other goals.


\item Graphical Representation of Affinity Networks
\\* During the user group interaction one of the key points established was that if the opportunity arose that despite the complexity in implementation further graphical representations of models were of high priority amongst the additional functionalities. In previous documentation and presentations for Cpi some relatively simple, smaller Cpi models have been represented in affinity networks to allow for a visualization of the model and the interactions between its components. Each of these examples has been created individually and whilst following similar layouts did not follow a dedicated specification. This lack of outlined visual notation for the language makes the implementation of this feature complicated. Before implementation a comprehensive notation would need to be designed and documented. The time required for the design of a comprehensive specification for expressing the model task makes the implementation of this functionality difficult. When the user group were asked about the this representation they believed it would be a very useful for using in development of models and for presenting their experiment to others. This led to a discussion about the notations used by the group in other tools. One of which is SBGN.

\item SBGN
From the user group feedback further graphical representations were found to be the most desired additional feature to extend the application. The proposed additional feature to add such a graphical representation in the planning report was a graphical notation for affinity network representations of the models. This is not as straightforward as any of the other goals as there is no existing notation. One alternative proposed is the use of an existing notation that is already developed and documented. SBGN has been used by more then one member of the user group and was suggested as an appropriate notation for the model representations. Whilst this would replace the need to design an new notation, SBGN does not directly translate the Cpi notation. To convert the affinity networks a process of translation from the Cpi definitions to SBGN would needed to succinctly express the models. This is still a complex task and may even be more difficult as if a new notation was created then it would not be restricted by the constraints of the SBGN notation.
\end{itemize}

\subsection{Summary}
From the possible goals listed above the decision was made to set which of these will be implemented for next semester and in what order. Below is a list of goals intended for the phase 2:
\begin{figure*}[t]
\centering
\begin{tabular}{ | p{0.1\textwidth} | p{0.845\textwidth} | }
\hline
\multicolumn{2}{ |c| }{Monthly Goals} \\
\hline
\multirow{3}{*}{September} & Implement tabled results for model simulations.\\
& Include capabilities to export entire model including results to file using CSV.\\\hline
\multirow{2}{*}{October} & Improve interface and prepare tool for user testing, ensure tool is robust and the application is tidy and intuitive. \\
& Better represent model definitions from all features in treeView widgets.\\
 & Implement tutorial for tool stepping through the model analysis. \\\hline
\multirow{2}{*}{November} & Approach user group with tool to test application\\
 & From meeting with the user group ascertain any alterations/additions that would be beneficial to the  tool.\\
 & Allow for implementation of changes from plan resulting in user feedback. \\\hline
\multirow{2}{*}{December} & Implementation of formula based model checking using text input.\\
 & Allow time for slippage \\\hline
\multirow{2}{*}{January}
 & Begin implementation of phase plot representation. \\
 & Possibly meet with users again to gain more feedback for evaluation of project\\
 & If successful begin specifying notation for affinity networks or SBGN\\\hline
\multirow{2}{*}{February} & Prepare evaluation of project for last group presentation. \\
 & Begin working only on dissertation  \\\hline
\multirow{1}{*}{March} & Continue working on dissertation  \\\hline
\multirow{1}{*}{April} & Hand in dissertation \\\hline
\end{tabular}
\caption{Monthly Goals for Phase 2 of the Project}
\end{figure*}


\begin{itemize}
\item Tabular representation of simulations
\\*As a key goal specified in the planning report this is the highest priority.
\item Further Experiment exportation
\\*This should require minimal time to implement and was specified as a beneficial addition by the user group.
\item Interface improvements
\\*The tool is ready for user testing at the beginning of the next phase will allow for possible user group meetings to gain important further feedback on implementation.
\item User tutorial
\\*Implementing a tutorial walk-through combined with the improvements in the interface should allow for more meaningful feed back from user testing.
\item{Formula based model checking}
\\*This will ensure that the tool encompasses all the functionality of the CpiWB as well as the additional capabilities and accessibility of a GUI and will provide a more focused form of simulation analysis.
\end{itemize}
If these goals are achieved two of the potential additional goals will be the focus of the implementation
\begin{itemize}
\item Phase plots
\\*This is the most feasible of the possible extensions and will provide another from of analysis for simulations.
\item Query builder for model checking
\\*This can be implemented as an extension of the text based model checking and due to the complexity it may only by feasible to provide a feature that covers some but not all possible queries within the time-frame of the next phase. 
\end{itemize}
If these goals are achieved then it may be possible to begin attempting to specify a graphical notation, either starting from scratch or by translating the affinity network definitions into SBGN.


\pagebreak

\section{Workplan}
This section aims to outline the implementation plan for the project leading to its completion.
\subsection{Time-line}
Figure 7 shows a breakdown of the intended goals specified in section 6. The table shows the intended goals for each month of the next phase in the project. 
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
\pagebreak
\bibliographystyle{plain}
\bibliography{references}

\end{document}
