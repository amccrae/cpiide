
import System.IO
import Graphics.UI.Gtk hiding (disconnect)
import Graphics.UI.Gtk hiding (get)
import Graphics.UI.Gtk.Glade
import Graphics.UI.Gtk.ModelView as Model
import Graphics.UI.Gtk.SourceView
import Graphics.UI.Gtk.Layout.Table
import Control.Concurrent.MVar
import Numeric.LinearAlgebra
import Graphics.UI.Gtk.Gdk.EventM
import Graphics.UI.Gtk.MenuComboToolbar.ComboBox
import Data.List
import Data.List.Split 
import CPi.Lib
import CPi.Parser
import CPi.Semantics
import CPi.ODE
import CPi.Plot
import CPi.Logic
import CPi.Matlab
import CPi.Signals
import System.GIO.File.AppInfo
import qualified Control.Monad.State.Strict as Strict
import Control.Monad.State.Strict 
import qualified Data.List as L
import qualified Numeric.LinearAlgebra as LA
import Data.String.Utils as Utils
import Data.Tuple
import Graphics.UI.Gtk as GTK

main :: IO ()
main =
    do	
	initGUI 
        defs <- newVar []
        gui <- loadGlade "editor.glade"
	editor gui defs


	mainGUI
	

editor :: GUI -> Barrier [Definition] -> IO()
editor gui defs = do

		lm <- sourceLanguageManagerNew
		sourceLanguageManagerSetSearchPath lm (Just ["/home/andrew/project/Cpi-IDE/lang/"])      
  		langM <- sourceLanguageManagerGetLanguage lm "cpi"
		
  		lang <- case langM of
    			(Just lang) -> return lang
    			Nothing -> do
				--sourceLanguageManagerSetSearchPath lm (Just [" "])      
      				langDirs <- sourceLanguageManagerGetSearchPath lm
      				error ("please copy hapskell.lang to one of the following directories:\n" ++unlines langDirs)

  			-- create a new SourceBuffer object
  		buffer <- sourceBufferNewWithLanguage lang

  -- load up and display a file
  		--fileContents <- readFile "SourceView.hs"
  		--textBufferSetText buffer fileContents
  		textBufferSetModified buffer False

  		sourceBufferSetHighlightSyntax buffer True
		
		mainText <- sourceViewNewWithBuffer buffer


		 
		gutter <- sourceViewGetGutter mainText TextWindowLeft
 		cell   <- cellRendererTextNew
  		sourceGutterInsert gutter cell 0
		-- Set gutter data.
		sourceGutterSetCellDataFunc gutter cell $ \ c l currentLine -> do
			-- Display line number.
			set (castToCellRendererText c) [cellText := show (l + 1)]
			-- Highlight current line.
			let color = if currentLine 
		                	then Color 65535 0 0 
		                    	else Color 0 65535 0
		 	set (castToCellRendererText c) [cellTextForegroundColor := color]

	  		-- Set gutter size.
  		sourceGutterSetCellSizeFunc gutter cell $ \ c -> 
      		-- -1 mean cell renderer will adjust width with chars dynamically.
      			set (castToCellRendererText c) [cellTextWidthChars := (2)]
		widgetSetSizeRequest mainText 260 300
		boxPackStartDefaults (hbox1 gui) mainText 

		boxReorderChild (hbox1 gui) mainText   0
		connectGui gui defs mainText
		widgetShowAll (hbox1 gui)



loadGlade :: FilePath -> IO GUI
loadGlade gladepath =
    do
       
        Just xml <- xmlNew gladepath
        mw <- xmlGetWidget xml castToWindow "MainWindow"
        windowSetTitle mw "CPIDE"
        windowResize mw 700 400
--menu
        [fileOpen,fileSaveAs,fileQuit,onlinemodelconnection] <- mapM (xmlGetWidget xml castToImageMenuItem) ["FileOpen","FileSaveAs","FileQuit","Onlinemodelconnection"]
        staticlabel<- xmlGetWidget xml castToLabel "staticlabel"
        odelabel<- xmlGetWidget xml castToLabel "odelabel"
	plotlabel <- xmlGetWidget xml castToLabel "plotlabel"
	modellabel <- xmlGetWidget xml castToLabel "modellabel"
	tablabel <- xmlGetWidget xml castToLabel "tablabel"
	phaselabel <- xmlGetWidget xml castToLabel "phaselabel"
	qBLabel <- xmlGetWidget xml castToLabel "qBLabel"
	dndLbl <-xmlGetWidget xml castToLabel "dndLbl"

	parse <- xmlGetWidget xml castToToolButton "parseButton"
	ode <- xmlGetWidget xml castToToolButton "odeButton"
	test <- xmlGetWidget xml castToToolButton "testBtn"
	plotButton <- xmlGetWidget xml castToButton "plotButton"
	mdlChk <- xmlGetWidget xml castToToolButton "mdlChkButton"
	phaseButton <- xmlGetWidget xml castToButton "phaseButton"
	tabButton <- xmlGetWidget xml castToButton "tabButton"
	csvButton <- xmlGetWidget xml castToButton "csvButton"
	addFormBtn <- xmlGetWidget xml castToButton "addFormBtn"
	trueBtn <- xmlGetWidget xml castToButton "trueBtn"
	falseBtn <- xmlGetWidget xml castToButton "falseBtn"
	gTBtn <- xmlGetWidget xml castToButton "gTBtn"
	gTEBtn <- xmlGetWidget xml castToButton "gTEBtn"
	lTBtn <- xmlGetWidget xml castToButton "lTBtn"
	lTEBtn <- xmlGetWidget xml castToButton "lTEBtn"
	eqBtn <- xmlGetWidget xml castToButton "eqBtn"
	nEqBtn <- xmlGetWidget xml castToButton "nEqBtn"

	andBtn <- xmlGetWidget xml castToButton "andBtn"
	orBtn <- xmlGetWidget xml castToButton "orBtn"
	impBtn <- xmlGetWidget xml castToButton "impBtn"
	notBtn <- xmlGetWidget xml castToButton "notBtn"
	untilBtn <- xmlGetWidget xml castToButton "untilBtn"
	relsBtn <- xmlGetWidget xml castToButton "relsBtn"
	necBtn <- xmlGetWidget xml castToButton "necBtn"
	gTeeBtn <- xmlGetWidget xml castToButton "gTeeBtn"
	logicBtn <- xmlGetWidget xml castToButton "logicBtn"	
	boolBtn <- xmlGetWidget xml castToButton "boolBtn"
	valBtn <- xmlGetWidget xml castToButton "valBtn"
	relBtn <- xmlGetWidget xml castToButton "relBtn"
	ariBtn <- xmlGetWidget xml castToButton "ariBtn"
	addBtn2 <- xmlGetWidget xml castToButton "addBtn2"
	onlineAddBtn <- xmlGetWidget xml castToButton "onlineAddBtn"


--	editProcess <- xmlGetWidget xml castToImageMenuItem "EditProcess"
--textview
       -- mainText <- xmlGetWidget xml castToTextView "mainText"
        textview1 <- xmlGetWidget xml castToTextView "textview1"
	textview2 <- xmlGetWidget xml castToTextView "textview2"
	textview3 <- xmlGetWidget xml castToTextView "textview3"

	textview5 <- xmlGetWidget xml castToTextView "textview5"
	textview6 <- xmlGetWidget xml castToTextView "textview6"
        modelTextInView <- xmlGetWidget xml castToTextView "modelTextInView"
        modelTextOutView <- xmlGetWidget xml castToTextView "modelTextOutView"

--        treeView1 <- xmlGetWidget xml castToTreeView "treeView1"
--        treeView2 <- xmlGetWidget xml castToTreeView "treeView2"

--treeview
	vbox6 <- xmlGetWidget xml castToVBox "vbox6"
	hbox1 <- xmlGetWidget xml castToHBox "hbox1"
	hbox2 <- xmlGetWidget xml castToHBox "hbox2"
	hbox15 <- xmlGetWidget xml castToHBox "hbox15"
	table1 <- xmlGetWidget xml castToTable "table1"
	hbox18 <- xmlGetWidget xml castToHBox "hbox18"
	vbox13 <- xmlGetWidget xml castToVBox "vbox13"
	vbox14 <- xmlGetWidget xml castToVBox "vbox14"
	vbox10 <- xmlGetWidget xml castToVBox "vbox10"
	hbox19 <- xmlGetWidget xml castToHBox "hbox19"
	dropHBox <- xmlGetWidget xml castToHBox	"dropHBox"

  	
-- drawing area
        drawArea <- xmlGetWidget xml castToDrawingArea "drawingarea1"
	drawArea `onSizeRequest` return (Requisition 400 400)
        drawArea2 <- xmlGetWidget xml castToDrawingArea "drawingarea2"
	drawArea2 `onSizeRequest` return (Requisition 400 400)

-- Entry boxes
	plotStartEntry  <- xmlGetWidget xml castToEntry "plotStartEntry"
	plotEndEntry  <- xmlGetWidget xml castToEntry "plotEndEntry"
	plotPointsEntry  <- xmlGetWidget xml castToEntry "plotPointsEntry"
	tabStartEntry  <- xmlGetWidget xml castToEntry "tabStartEntry"
	tabEndEntry  <- xmlGetWidget xml castToEntry "tabEndEntry"
	tabPointsEntry   <- xmlGetWidget xml castToEntry "tabPointsEntry"
	phaseStartEntry   <- xmlGetWidget xml castToEntry "phaseStartEntry"
	phaseEndEntry   <- xmlGetWidget xml castToEntry "phaseEndEntry"
	phasePointsEntry   <- xmlGetWidget xml castToEntry "phasePointsEntry"
	formulaEntry <- xmlGetWidget xml castToEntry "formulaEntry"
	logicEntry <- xmlGetWidget xml castToEntry "logicEntry"
	boolEntry <- xmlGetWidget xml castToEntry "boolEntry"
	valEntry <- xmlGetWidget xml castToEntry "valEntry"
	relEntry <- xmlGetWidget xml castToEntry "relEntry"
	ariEntry <- xmlGetWidget xml castToEntry "ariEntry"
	onlineNameEntry <- xmlGetWidget xml castToEntry "onlineNameEntry"
	onlineIDEntry <- xmlGetWidget xml castToEntry "onlineIDEntry"
	onlineURLEntry <- xmlGetWidget xml castToEntry "onlineURLEntry"


--	phaseProcessEntry <- xmlGetWidget xml castToEntry "phaseProcessEntry"

-- combobox 
	plotProComboBox <- xmlGetWidget xml castToComboBox "plotProCombobox"
	tabProCombobox <- xmlGetWidget xml castToComboBox "tabProCombobox"
	phaseProCombobox <- xmlGetWidget xml castToComboBox "phaseProCombobox"
	plotSpec1ComboBox <- xmlGetWidget xml castToComboBox "plotSpec1Combobox"
	phaseSpecies1Combobox <- xmlGetWidget xml castToComboBox "phaseSpecies1Combobox"
	phaseSpecies2Combobox <- xmlGetWidget xml castToComboBox "phaseSpecies2Combobox"




--opener
        opener <- xmlGetWidget xml castToFileChooserDialog "openFileDialog"
        openerBtn <- xmlGetWidget xml castToButton "openerBtn"
        openerBtnCancel <- xmlGetWidget xml castToButton "openerBtnCancel"
        
--saver
        saviour <- xmlGetWidget xml castToFileChooserDialog "saveFileDialog"
        saviourBtn <- xmlGetWidget xml castToButton "saviourBtn"
        saviourBtnCancel <- xmlGetWidget xml castToButton "saviourBtnCancel"


	andDialog <- xmlGetWidget xml castToDialog "andDialog"
	andEntry1 <- xmlGetWidget xml castToEntry "andEntry1"
	andEntry2 <- xmlGetWidget xml castToEntry "andEntry2" 
	entry1 <- xmlGetWidget xml castToEntry "entry1"      
	andDoneBtn <- xmlGetWidget xml castToButton "andDoneBtn"
	andCancelBtn <- xmlGetWidget xml castToButton "andCancelBtn"
	onlineDialog <- xmlGetWidget xml castToDialog "OnlineDialog"
	
	newBtn <- xmlGetWidget xml castToButton "newBtn"
	formulaCombineBtn <- xmlGetWidget xml castToButton "formulaCombineBtn"


--pack 
	return $ GUI mw
        	fileOpen 
		fileSaveAs 
		fileQuit
		onlinemodelconnection
          --      mainText
		textview1
		textview2
		textview5
		textview6
		modelTextInView
		modelTextOutView
--		treeView1	
--		treeView2	
		drawArea
                opener 
                openerBtn 
                openerBtnCancel                   
                saviour 
                saviourBtn 
                saviourBtnCancel
		parse
		ode
		test
	        plotButton 
		mdlChk
		staticlabel
		odelabel
		plotlabel
		modellabel
		tablabel
		phaselabel
		qBLabel
		dndLbl
		plotStartEntry
		plotEndEntry
		plotPointsEntry
		tabStartEntry
		tabEndEntry
		tabPointsEntry 
		phaseStartEntry 
		phaseEndEntry 
		phasePointsEntry 
		drawArea2
		phaseButton		
		tabButton
		csvButton
		vbox6
		hbox1
		hbox2
		hbox15
		hbox18
		vbox13
		vbox14
		vbox10
		hbox19
		dropHBox
		table1
		plotProComboBox
		tabProCombobox
		phaseProCombobox
		plotSpec1ComboBox
		phaseSpecies1Combobox
		phaseSpecies2Combobox
		newBtn
		trueBtn
		falseBtn
		gTBtn
		gTEBtn
		lTBtn
		lTEBtn
		eqBtn
		nEqBtn
		andBtn
		orBtn
		impBtn
		notBtn
		untilBtn
		relsBtn
		necBtn
		gTeeBtn
		addFormBtn
		formulaEntry
		andDialog 
		andEntry1
		andEntry2
		andDoneBtn
		andCancelBtn
		textview3
		logicBtn
		logicEntry
		boolBtn
		valBtn
		relBtn
		ariBtn
		addBtn2
		boolEntry
		valEntry
		relEntry
		ariEntry
		onlineDialog
		onlineNameEntry
		onlineIDEntry
		onlineURLEntry
		onlineAddBtn
		formulaCombineBtn
		entry1

                   
--Gui data struct                    
data GUI = GUI {
        mainWin :: Window,
        fileOpen :: ImageMenuItem,
        fileSaveAs :: ImageMenuItem,
        fileQuit :: ImageMenuItem,
	onlinemodelconnection :: ImageMenuItem,
       -- mainText :: TextView,
	textview1 :: TextView,
	textview2 :: TextView,
	textview5 :: TextView,
	textview6 :: TextView,
	modelTextInView :: TextView,
	modelTextOutView :: TextView,
--	treeView1 :: TreeView,
--	treeView2 :: TreeView,
	drawArea :: DrawingArea,
        opener :: FileChooserDialog,
        openerBtn :: Button,
        openerBtnCancel :: Button,
        saviour :: FileChooserDialog,
        saviourBtn :: Button,
        saviourBtnCancel :: Button,
	parse :: ToolButton,
	ode :: ToolButton,
	test :: ToolButton,
	plotButton :: Button,
	mdlChk :: ToolButton,
	staticlabel :: Label,
	odelabel :: Label,
	plotlabel :: Label,
	modellabel :: Label,
	tablabel :: Label,
	phaselabel :: Label,
	qBLabel :: Label,
	dndLbl :: Label,
	plotStartEntry :: Entry,
	plotEndEntry :: Entry,
	plotPointsEntry :: Entry,
	tabStartEntry :: Entry,
	tabEndEntry :: Entry,
	tabPointsEntry :: Entry,
	phaseStartEntry :: Entry,
	phaseEndEntry :: Entry,
	phasePointsEntry :: Entry,
	drawArea2 :: DrawingArea,
	phaseButton :: Button,
	tabButton :: Button,
	csvButton :: Button,
	vbox6 :: VBox,
	hbox1 :: HBox,
	hbox2 :: HBox,
	hbox15 :: HBox,
	hbox18 :: HBox,
	vbox13 :: VBox,
	vbox14 :: VBox,
	vbox10 :: VBox,
	hbox19 :: HBox,
	dropHBox :: HBox,
	table1 :: Table,
	plotProComboBox :: ComboBox,
	tabProComboBox :: ComboBox,
	phaseProComboBox :: ComboBox,
	plotSpec1ComboBox :: ComboBox,
	phaseSpecies1Combobox :: ComboBox,
	phaseSpecies2Combobox :: ComboBox,
	newBtn :: Button,
	trueBtn :: Button,
	falseBtn :: Button,
	gTBtn :: Button,
	gTEBtn :: Button,
	lTBtn :: Button,
	lTEBtn :: Button,
	eqBtn :: Button,
	nEqBtn :: Button,
	andBtn :: Button,
	orBtn :: Button,
	impBtn :: Button,
	notBtn :: Button,
	untilBtn :: Button,
	relsBtn :: Button,
	necBtn :: Button,
	gTeeBtn :: Button,
	addFormBtn :: Button,
	formulaEntry :: Entry,
	andDialog :: Dialog,
	andEntry1 :: Entry,
	andEntry2 :: Entry,
	andDoneBtn :: Button,
	andCancelBtn :: Button,
	textview3 :: TextView,
	logicBtn :: Button,
	logicEntry :: Entry,
	boolBtn :: Button,
	valBtn :: Button,
	relBtn :: Button,
	ariBtn :: Button,
	addBtn2 :: Button,
	boolEntry :: Entry,
	valEntry :: Entry,
	relEntry :: Entry,
	ariEntry :: Entry,
	onlineDialog :: Dialog,
	onlineNameEntry :: Entry,
	onlineIDEntry:: Entry,
	onlineURLEntry :: Entry,
	onlineAddBtn :: Button,
	formulaCombineBtn :: Button,
	entry1 :: Entry
    }
      
--handler


connectGui :: GUI -> Barrier [Definition]  -> SourceView -> IO (ConnectId Button)
connectGui gui defs mainText =
    do
        onDestroy (mainWin gui) mainQuit

        {-onExpose drawArea $ \e -> do 
	  win <- drawingAreaGetDrawWindow drawArea 
	  renderWithDrawable win $ do 
	    showText "Hello Wlist <- listStoreNew []
			treeview <- treeViewNewWithModel list
			treeViewSetHeadersVisible treeview True
			col <- treeViewColumnNew
			treeViewColumnSetTitle col (spec!!(x))
			renderer <- cellRendererTextNew
			printStringNTimes x list solns (rows solns) (rows solns)
			cellLayoutPackStart col renderer False
			cellLayoutSetAttributes col renderer list
				$ \ind -> [cellText := ind]
			treeViewAppendColumn treeview col
			tree <- treeViewGetSelection treeview
			treeSelectionSetMode tree SelectionBrowse --Multiple
			boxPackStartDefaults hbox2 treeview
			boxReorderChild hbox2 treeview xorld!"
-}
        onActivateLeaf (fileOpen gui) $ windowPresent (opener gui) --openFile1 gui
        onActivateLeaf (fileSaveAs gui) $ saveAs gui
        onActivateLeaf (fileQuit gui) mainQuit
        onActivateLeaf (onlinemodelconnection gui) $ connectionsWindow gui

        onToolButtonClicked (parse gui ) $ parseClicked gui defs mainText
 	onToolButtonClicked (ode gui) $ odeClicked gui defs
	onToolButtonClicked (test gui) $ testClicked gui defs mainText
 	onClicked (plotButton gui) $ plotClicked gui defs
        onToolButtonClicked (mdlChk gui) $ modelCheckClicked gui defs
	onClicked (tabButton gui) $ tableClicked gui defs
	onClicked (csvButton gui) $ csvClicked gui defs
	onClicked (phaseButton gui) $ phaseClicked gui defs
--opener 
        onClicked (openerBtn gui) $ openerBtnClicked gui mainText
        onClicked (openerBtnCancel gui) $ widgetHide $ opener gui
--saver       
        onClicked (saviourBtn gui) $ saviourSaveBtnClicked gui mainText
        onClicked (saviourBtnCancel gui) $ widgetHide $ saviour gui
--	onClicked (but gui) $ rClicked gui
	onClicked (trueBtn gui ) $ trueClicked gui defs
	onClicked (falseBtn gui ) $ falseClicked gui defs
        onClicked (andBtn gui ) $ windowPresent (andDialog gui) 

	onClicked (andDoneBtn gui ) $ andDoneClicked gui defs
	onClicked (addFormBtn gui ) $ addFormClicked gui defs
	(logicBtn gui ) `on` buttonActivated $ logicClicked gui
	(newBtn gui ) `on` buttonActivated $ alter gui defs
	(boolBtn gui ) `on` buttonActivated $ boolClicked gui
	dragDestSet (logicBtn gui) [DestDefaultMotion, DestDefaultDrop] [ActionCopy]
   	dragDestAddTextTargets (logicBtn gui)
	(logicBtn gui) `on` dragDataReceived $ \dc pos id ts -> do
        	s <- selectionDataGetText
        	liftIO . putStrLn $ case s of
            		Nothing -> "didn't understand the drop"
            		Just s  -> "understood, here it is: <" ++ s ++ ">"

	onClicked (onlineAddBtn gui ) $ onlineAddClicked gui defs
	(formulaCombineBtn gui) `on` buttonActivated $ formComb gui
	--onClicked (logicBtn gui) $ logicClicked
	--changed (phaseProComboBox gui) $ phaseSpeciesFill gui defs
--opener window 



alter :: GUI -> Var [Definition] -> IO()
alter gui defs = 
	do 
	sourceView <- sourceViewNew
	boxPackStartDefaults (vbox14 gui) sourceView
	widgetShowAll (vbox14 gui)
--	(formulaCombineBtn gui) `on` buttonActivated $ formComb gui
	containerRemove (dropHBox gui) (newBtn gui)
	collectComboT gui defs sourceView


formComb :: GUI -> IO()
formComb gui = do
			box <- containerGetChildren (hbox19 gui)		
			print "yeh"


removeHBoxT :: GUI -> HBox -> Var [Definition] -> SourceView -> IO()
removeHBoxT gui hb defs sv  =
			do
			containerRemove (dropHBox gui) hb
			initComboT gui defs sv



removeHBox :: GUI -> HBox -> Var [Definition] -> SourceView -> IO()
removeHBox gui hb defs sv =
			do
			containerRemove (dropHBox gui) hb
			initComboTV gui defs sv

collectTim :: GUI -> HBox -> ComboBox ->  Entry -> Entry -> Var [Definition] -> SourceView -> IO()
collectTim gui hb cb f t  defs sourceView = do
	
	c <- comboBoxGetActiveText cb 
	case c of
		Nothing -> return()
        	Just c  -> do
				from <- entryGetText f
				if (isInteger from) then do
					to <- entryGetText t
					if (isInteger to) && ((read to::Int) > (read from::Int)) then do
						let str = c ++ "{" ++ from ++ "," ++ to ++ "}( "
						timLbl <- labelNew Nothing
						labelSetText timLbl str
						entryAppendText (entry1 gui) str


						
						
						box <- containerGetChildren (dropHBox gui)
						mapM_ (containerRemove (dropHBox gui)) box
						leftBtn <- buttonNewWithLabel "<-"
						leftBtn `on` buttonActivated $ removeLblT gui leftBtn timLbl defs sourceView
						initComboTV gui defs sourceView
						boxPackStartDefaults (hbox19 gui) leftBtn
						boxReorderChild (hbox19 gui) (dropHBox gui) 0				
						boxPackStartDefaults (hbox19 gui) timLbl
						boxReorderChild (hbox19 gui) (dropHBox gui) 1
						box2 <- containerGetChildren (hbox19 gui)
						let braks =  div (length(box2)-1) 2
						print braks
						buff <- textViewGetBuffer sourceView
						si <- textBufferGetStartIter buff
        					ei <- textBufferGetEndIter buff
						b <- textIterBackwardChar ei
						let n = take braks (repeat ')') 
						text <- textBufferGetText buff si ei False
						textBufferSetText buff (text ++ str ++ n)
													
						let x = (length box2)
						boxReorderChild (hbox19 gui) (dropHBox gui) x
						widgetShowAll (hbox19 gui)
					else return()
				else return()
--	to <- entryGetText (boxChild!!(1))
--	print (from )

removeLbl gui leftBtn lbl defs sv = do
					containerRemove (hbox19 gui) leftBtn
					containerRemove (hbox19 gui) lbl
					widgetShowAll (hbox19 gui)
					initComboTV gui defs sv

removeLblO gui leftBtn lbl defs sv = do
					
					
					str <- labelGetText lbl
					txt <- entryGetText (entry1 gui)
					let t = Utils.replace  str " " txt
					let txt = t
					entrySetText (entry1 gui) txt
					p <- GTK.get (hbox19 gui) (boxChildPosition leftBtn) 
					box <- containerGetChildren (hbox19 gui)
					print $length(box)
					let b = slice p (length(box) -2) box
					mapM_ (containerRemove (hbox19 gui)) b
					widgetShowAll (hbox19 gui)
					box <- containerGetChildren (hbox19 gui)
					if (length box) < 2 then do 
								box2 <- containerGetChildren (dropHBox gui)
								mapM_ (containerRemove (dropHBox gui)) box2
								initComboO gui defs sv
					else 
						return ()

slice from to xs = take (to - from + 1) (drop from xs)


removeLblT gui leftBtn timLbl defs sv  = do
					str <- labelGetText timLbl
					txt <- entryGetText (entry1 gui)
					let t = Utils.replace  str " " txt
					let txt = t
					entrySetText (entry1 gui) txt
					p <- GTK.get (hbox19 gui) (boxChildPosition leftBtn) 
					box <- containerGetChildren (hbox19 gui)
					print $length(box)
					let b = slice p (length(box) -2) box
					mapM_ (containerRemove (hbox19 gui)) b
					widgetShowAll (hbox19 gui)
					box <- containerGetChildren (hbox19 gui)
					if (length box) < 2 then do 
								box2 <- containerGetChildren (dropHBox gui)
								mapM_ (containerRemove (dropHBox gui)) box2
								initComboT gui defs sv
					else 
						return ()



initComboTV :: GUI -> Var [Definition] -> SourceView -> IO ()
initComboTV gui defs sv  = do
			cb <- comboBoxNewText			
			vtBtn <- buttonNewWithLabel "yep"
			comboBoxAppendText cb "Time"
			comboBoxAppendText cb "Value"
			boxPackEndDefaults (dropHBox gui) vtBtn
			boxPackEndDefaults (dropHBox gui) cb
			vtBtn  `on` buttonActivated $ collectComboTV gui cb defs sv
			widgetShowAll (dropHBox gui)

initComboT :: GUI -> Var [Definition] -> SourceView -> IO ()
initComboT gui defs sv = do
			cb <- comboBoxNewText			
			tBtn <- buttonNewWithLabel "yep"
			comboBoxAppendText cb "Time"
			boxPackEndDefaults (dropHBox gui) tBtn
			boxPackEndDefaults (dropHBox gui) cb
			tBtn  `on` buttonActivated $ collectComboT gui defs sv
			widgetShowAll (dropHBox gui)

initComboV :: GUI -> Var [Definition] -> SourceView -> IO ()
initComboV gui defs  sv = do
			cb <- comboBoxNewText			
			vBtn <- buttonNewWithLabel "yep"
			comboBoxAppendText cb "Value"
			boxPackEndDefaults (dropHBox gui) vBtn
			boxPackEndDefaults (dropHBox gui) cb
			vBtn  `on` buttonActivated $ collectComboV gui defs sv
			widgetShowAll (dropHBox gui)


initComboO :: GUI -> Var [Definition] -> SourceView -> IO ()
initComboO gui defs  sv = do
			cb <- comboBoxNewText			
			vtBtn <- buttonNewWithLabel "yep"
			comboBoxAppendText cb "Operator"

			boxPackEndDefaults (dropHBox gui) vtBtn
			boxPackEndDefaults (dropHBox gui) cb
			vtBtn  `on` buttonActivated $ collectComboO gui defs sv 
			widgetShowAll (dropHBox gui)

collectComboO :: GUI -> Var [Definition] -> SourceView -> IO()
collectComboO gui defs sv = do
			hb <- hBoxNew False 3
			box <- containerGetChildren (dropHBox gui)
			mapM_ (containerRemove (dropHBox gui)) box

			comboAddOp gui hb defs sv 
			boxPackStartDefaults (dropHBox gui) hb

comboAddOp :: GUI -> HBox -> Var [Definition] -> SourceView -> IO()
comboAddOp gui hb defs sv = do
	cb <- comboBoxNewText
	addOp cb
	cb `on` changed $ hotSwapComboO gui cb hb defs sv
	leftBtn <- buttonNewWithLabel "<-"
	boxPackStartDefaults hb leftBtn
	boxPackStartDefaults hb cb
	leftBtn `on` buttonActivated $ removeHBox gui hb defs sv
	widgetShowAll hb



comboAddVal :: GUI -> HBox -> Var [Definition] -> SourceView -> IO()
comboAddVal gui hb defs sv = do
	cb <- comboBoxNewText
	addVal cb

	cb `on` changed $ hotSwapComboTV gui cb hb defs sv 
	leftBtn <- buttonNewWithLabel "<-"
	boxPackStartDefaults hb leftBtn
	boxPackStartDefaults hb cb
	leftBtn `on` buttonActivated $ removeHBox gui hb defs sv
	widgetShowAll hb


hotSwapComboO :: GUI -> ComboBox -> HBox -> Var [Definition] -> SourceView -> IO()
hotSwapComboO gui cb hb defs sv = do 
			i <- comboBoxGetActive cb
			box <- containerGetChildren hb
			if length(box) > 3 then do
				containerRemove hb (box!!(2))
				containerRemove hb (box!!(3))
			else return()
			if i == 0 then do 
					cb <- comboBoxNewText
					addRel cb
					relBtn <- buttonNewWithLabel "Add"
					relBtn  `on` buttonActivated $ collectOpR gui defs cb sv 
					boxPackStartDefaults hb cb 
					boxPackStartDefaults hb relBtn
					boxReorderChild hb cb 2
					widgetShowAll hb
			else do		
					cb <- comboBoxNewText
					addAri cb
					ariBtn <- buttonNewWithLabel "Add"
					ariBtn  `on` buttonActivated $ collectOpA gui defs cb sv 
					boxPackStartDefaults hb cb 
					boxPackStartDefaults hb ariBtn
					boxReorderChild hb cb 2
					widgetShowAll hb


collectOpR :: GUI ->  Var [Definition] -> ComboBox -> SourceView -> IO()
collectOpR gui defs cb sv = do 
				i <- comboBoxGetActive cb
				let optns = ["Greater Than","Less Than","Equals"]
				opLbl <- labelNew Nothing
				labelSetText opLbl $ optns!!(i)
				entryAppendText (entry1 gui) $ optns!!(i)
				box <- containerGetChildren (dropHBox gui)
				mapM_ (containerRemove (dropHBox gui)) box
				leftBtn <- buttonNewWithLabel "<-"
				leftBtn `on` buttonActivated $ removeLblO gui leftBtn opLbl defs sv
				initComboV gui defs sv
				boxPackStartDefaults (hbox19 gui) leftBtn
				boxReorderChild (hbox19 gui) (dropHBox gui) 0				
				boxPackStartDefaults (hbox19 gui) opLbl
				boxReorderChild (hbox19 gui) (dropHBox gui) 1
				box2 <- containerGetChildren (hbox19 gui)
				let x = (length box2)
				boxReorderChild (hbox19 gui) (dropHBox gui) x
				widgetShowAll (hbox19 gui)


collectOpA :: GUI ->  Var [Definition] -> ComboBox  -> SourceView -> IO()
collectOpA gui defs cb sv = do 
				i <- comboBoxGetActive cb
				let optns = ["+","-","x","/"]
				opLbl <- labelNew Nothing
				labelSetText opLbl $ optns!!(i)
				entryAppendText (entry1 gui) $ optns!!(i)

				box <- containerGetChildren (dropHBox gui)
				mapM_ (containerRemove (dropHBox gui)) box
				leftBtn <- buttonNewWithLabel "<-"
				leftBtn `on` buttonActivated $ removeLblO gui leftBtn opLbl defs sv
				initComboV gui defs sv
				boxPackStartDefaults (hbox19 gui) leftBtn
				boxReorderChild (hbox19 gui) (dropHBox gui) 0				
				boxPackStartDefaults (hbox19 gui) opLbl
				boxReorderChild (hbox19 gui) (dropHBox gui) 1
				box2 <- containerGetChildren (hbox19 gui)
				let x = (length box2)
				boxReorderChild (hbox19 gui) (dropHBox gui) x
				widgetShowAll (hbox19 gui)




collectComboTV :: GUI -> ComboBox -> Var [Definition] -> SourceView -> IO()
collectComboTV gui cb  defs sv  = do	
			i <- comboBoxGetActive cb
			if i == 0 then do
					collectComboT gui defs sv
			else do
				collectComboV gui defs sv

collectComboT :: GUI -> Var [Definition] -> SourceView -> IO()
collectComboT gui defs sv = do
			box <- containerGetChildren (dropHBox gui)
			mapM_ (containerRemove (dropHBox gui)) box
			hb <- hBoxNew False 3
			comboAddTim gui hb defs sv
			boxPackStartDefaults (dropHBox gui) hb
			

comboAddTim :: GUI -> HBox -> Var [Definition] -> SourceView-> IO()
comboAddTim gui hb defs sv = 
	do
	cb <- comboBoxNewText
	addTim cb		
	fromTim <- entryNew 
	entrySetText fromTim "From"
	toTim <- entryNew
	entrySetText toTim "To"
	timBtn <- buttonNewWithLabel "Add"
	leftBtn <- buttonNewWithLabel "<-"
	boxPackStartDefaults hb leftBtn
	boxPackStartDefaults hb cb
	boxPackStartDefaults hb fromTim
	boxPackStartDefaults hb toTim
	boxPackStartDefaults hb timBtn
	timBtn  `on` buttonActivated $ collectTim gui hb cb fromTim toTim defs sv
	leftBtn `on` buttonActivated $ removeHBoxT gui hb defs sv
	widgetShowAll hb 



collectComboV :: GUI -> Var [Definition] -> SourceView -> IO()
collectComboV gui defs sv = do
			hb <- hBoxNew False 3
			box <- containerGetChildren (dropHBox gui)
			mapM_ (containerRemove (dropHBox gui)) box

			comboAddVal gui hb defs sv 
			boxPackStartDefaults (dropHBox gui) hb



collectValS :: GUI ->  Var [Definition] -> [String] -> ComboBox -> SourceView -> IO()
collectValS gui defs sns cb sv = do 
				i <- comboBoxGetActive cb
				let s = sns!!(i)
				specLbl <- labelNew Nothing
				labelSetText specLbl $ "[" ++ s ++ "]"
				entryAppendText (entry1 gui) $ "[" ++ s ++ "]"
				box <- containerGetChildren (dropHBox gui)
				mapM_ (containerRemove (dropHBox gui)) box
				leftBtn <- buttonNewWithLabel "<-"
				leftBtn `on` buttonActivated $ removeLbl gui leftBtn specLbl defs sv
				initComboO gui defs sv 
				boxPackStartDefaults (hbox19 gui) leftBtn
				boxReorderChild (hbox19 gui) (dropHBox gui) 0				
				boxPackStartDefaults (hbox19 gui) specLbl
				boxReorderChild (hbox19 gui) (dropHBox gui) 1
				box2 <- containerGetChildren (hbox19 gui)
				let x = (length box2)
				boxReorderChild (hbox19 gui) (dropHBox gui) x
				widgetShowAll (hbox19 gui)
		

	


collectValN :: GUI -> HBox -> Entry -> Var [Definition] -> SourceView -> IO()
collectValN gui hb valEntry defs  sv = do
	val <- entryGetText valEntry
	if (isInteger val) then do
		valLbl <- labelNew Nothing
		labelSetText valLbl val
		entryAppendText (entry1 gui) val
		box <- containerGetChildren (dropHBox gui)
		mapM_ (containerRemove (dropHBox gui)) box
		leftBtn <- buttonNewWithLabel "<-"
		leftBtn `on` buttonActivated $ removeLblT gui leftBtn valLbl defs sv
		initComboO gui defs sv
		boxPackStartDefaults (hbox19 gui) leftBtn
		boxReorderChild (hbox19 gui) (dropHBox gui) 0				
		boxPackStartDefaults (hbox19 gui) valLbl
		boxReorderChild (hbox19 gui) (dropHBox gui) 1
		box2 <- containerGetChildren (hbox19 gui)
		let x = (length box2)
		boxReorderChild (hbox19 gui) (dropHBox gui) x
		widgetShowAll (hbox19 gui)
	else return()




hotSwapComboTV :: GUI -> ComboBox -> HBox -> Var [Definition] -> SourceView -> IO()
hotSwapComboTV gui cb hb defs sv = do 
			i <- comboBoxGetActive cb
			box <- containerGetChildren hb
			if length(box) > 3 then do
				containerRemove hb (box!!(2))
				containerRemove hb (box!!(3))
			else return()
			if i == 0 then do 
					tds <- readVar defs;
					let ps = pros tds;
					let pns = proNames tds
					let p = ps!!(0);
					let ss = speciesInProc p
					let sns = specNames tds
					cb <- comboBoxNewText
					addSpecs cb sns (length(sns)) (length(sns))
					valBtn <- buttonNewWithLabel "Add"
					valBtn  `on` buttonActivated $ collectValS gui defs sns cb sv
					
					boxPackStartDefaults hb cb 
					boxPackStartDefaults hb valBtn
					boxReorderChild hb cb 2
					widgetShowAll hb
			else do
				valEntry <- entryNew
				entrySetText valEntry "enter value"
				valBtn <- buttonNewWithLabel "Add"
				valBtn  `on` buttonActivated $ collectValN gui hb valEntry defs sv
				boxPackStartDefaults hb valEntry 
				boxPackStartDefaults hb valBtn
				boxReorderChild hb valEntry 2
				widgetShowAll hb




connectionsWindow :: GUI -> IO ()
connectionsWindow gui = do
			inf <- openFile ("connections.ocs") ReadMode
                    	inputData <- hGetContents inf
                    	buff <- textViewGetBuffer $ textview6 gui
                    	textBufferSetText buff inputData
                    	hClose inf
			windowPresent (onlineDialog gui)


openerBtnClicked :: GUI -> SourceView -> IO ()
openerBtnClicked gui mainText =
    do
        file <- fileChooserGetFilename (opener gui)
        case file of
            Just fpath -> loadFile (show fpath) gui
            Nothing -> widgetHide (opener gui)
        where
            loadFile fileName gui = 
                do
                    inf <- openFile (init  (tail fileName)) ReadMode
                    inputData <- hGetContents inf
                    buff <- textViewGetBuffer mainText 
                    textBufferSetText buff inputData
                    windowSetTitle (mainWin gui) (init(tail fileName) )
                    hClose inf
                    widgetHide (opener gui)        


--saver window 

saveAs :: GUI -> IO ()
saveAs gui =
    do
        fileChooserSetAction (saviour gui) FileChooserActionSave            
        windowPresent (saviour gui)


saviourSaveBtnClicked :: GUI -> SourceView -> IO ()
saviourSaveBtnClicked gui mainText =
    do
        file <- fileChooserGetFilename (saviour gui)
        case file of
            Just fpath -> save (init(tail(show fpath))) gui mainText
            Nothing -> widgetHide (opener gui)                        


save :: FilePath -> GUI -> SourceView -> IO ()
save fileName gui mainText =
    do
        outh <- openFile fileName WriteMode
        buff <- textViewGetBuffer mainText
        si <- textBufferGetStartIter buff
        ei <- textBufferGetEndIter buff
        text <- textBufferGetText buff si ei False        
        hPutStrLn outh text
        hClose outh
        widgetHide (saviour gui)
        windowSetTitle (mainWin gui) (fileName)
	textBufferSetModified buff False


testClicked :: GUI -> Var[Definition] -> SourceView -> IO()
testClicked gui defs mainText =
	do
	inf <- openFile ("models/testSimple.cpi") ReadMode
        inputData <- hGetContents inf
        buff <- textViewGetBuffer mainText
        textBufferSetText buff inputData



parseClicked :: GUI -> Var [Definition] -> SourceView -> IO ()
parseClicked gui defs mainText =
    do
      inf <- openFile ("connections.ocs") ReadMode
      inputData <- hGetContents inf
      --print inputData
      let connections = lines inputData
      let lineparts =  map (splitOn "|") (connections)
      let lineparts2 = map head (lineparts)
      --print lineparts	
      --print lineparts2	
      hClose inf
      buff <- textViewGetBuffer mainText 
      si <- textBufferGetStartIter buff
      ei <- textBufferGetEndIter buff
      text <- textBufferGetText buff si ei False
      inf <- openFile ("connections.ocs") ReadMode
      inputData <- hGetContents inf
      --print inputData
      let connections = lines inputData
      let lineparts =  map (splitOn "|") (connections)
      let lineparts2 = map head (lineparts)
      --print lineparts	
      --print lineparts2
      --print text 
      let matchedhashes = (hashes lineparts text)
      --print matchedhashes
      --openUrlBySystemTool "http://glade.gnome.org"
      --print ( isInfixOf "Andrew" text)
      showconnections (matchedhashes) gui 
      widgetShowAll (vbox13 gui)
      hClose inf
      case parseFile text of
                 Left err -> do buff <- textViewGetBuffer $ textview1 gui;
                                textBufferSetText buff (show "error")
            	 Right ds -> do buff <- textViewGetBuffer $ textview1 gui;
				--odeon <- newVar (pretty (ds!!((length ds)-1)));
				a <- takeMVar defs
		                putMVar defs ds
				tds <- readVar defs
				textBufferSetText buff (prettys tds)
				


pros :: [Definition] -> [Process]
pros [] = []
pros ((SpeciesDef _ _ _):xs) = pros xs 
pros ((ProcessDef _ proc):xs) = proc : (pros xs)

specs :: [Definition] -> [Species]
specs [] = []
specs ((ProcessDef _ _):xs) = (specs xs)
specs ((SpeciesDef _ _ s ):xs) = s: (specs xs)


specNames :: [Definition] -> [Name]
specNames [] = []
specNames ((ProcessDef _ _):xs) = (specNames xs)
specNames ((SpeciesDef s _ _):xs) = s: (specNames xs) 

proNames :: [Definition] -> [String]
proNames [] = []
proNames ((SpeciesDef _ _ _):xs) = proNames xs 
proNames ((ProcessDef  proc _):xs) = proc : (proNames xs)

spectablenames :: [(String,[Double])] -> [String]
spectablenames [] = []
spectablenames (( str, _ ):xs) = str:(spectablenames xs)

spectableval :: [(String,[Double])] -> [[Double]]
spectableval [] = []
spectableval ((_, vals):xs) = vals: (spectableval xs)

spectableN :: (String,[Double]) -> String
spectableN ( str, _) = str

spectableV :: (String,[Double]) -> [Double]
spectableV (_,vals) = vals


hashes :: [[String]] -> String -> [[String]]
hashes [] _ = []
hashes _ "" = []
hashes ((hsh):hshs) txt = if (isInfixOf ("#" ++(hsh!!(0))) txt) then 
				((hsh) : (hashes hshs txt))
			  else hashes hshs txt

openUrlBySystemTool :: String -> IO ()
openUrlBySystemTool url = do
  			infos <- appInfoGetAllForType "text/html"
			case infos of
			    [] -> return ()
			    xs -> appInfoLaunchUris (head xs) [url] Nothing

showconnections :: [[String]] -> GUI -> IO()
showconnections [] _ =  return ()
showconnections ((hsh):hshs) gui = do				
				boxtemp  <- vBoxNew False 0
				lBtntemp <- buttonNewWithLabel (hsh!!(0))
				connectBtn2Gui (hsh!!(2)) gui lBtntemp
				boxPackStartDefaults boxtemp lBtntemp
				boxPackStartDefaults (hbox18 gui) boxtemp
				showconnections hshs gui
				
				
				





odeClicked :: GUI -> Var [Definition] -> IO ()
odeClicked gui defs =
    do  tds <- readVar defs;
	if (tds /= []) then do
		let ps = pros tds;
		let pns = proNames tds
		let p = ps!!(0);
		let ss = speciesInProc p
		let sns = specNames tds
		let mts = processMTS tds p
		let dpdt = dPdt tds mts p
		buff2 <- textViewGetBuffer $ textview2 gui;
		list <- comboBoxSetModelText (plotProComboBox gui)
		addPros (plotProComboBox gui) pns (length(pns)) (length(pns))
		list1 <- comboBoxSetModelText (tabProComboBox gui)
		addPros (tabProComboBox gui) pns (length(pns)) (length(pns))
		list2 <- comboBoxSetModelText (phaseProComboBox gui)
		addPros (phaseProComboBox gui) pns (length(pns)) (length(pns))
		list3 <- comboBoxSetModelText (phaseSpecies1Combobox gui)
		addSpecs (phaseSpecies1Combobox gui) sns (length(sns)) (length(sns))
		list4 <- comboBoxSetModelText (phaseSpecies2Combobox gui)
		addSpecs (phaseSpecies2Combobox gui) sns (length(sns)) (length(sns))
		textBufferSetText buff2 (prettyODE tds dpdt )

		lm <- sourceLanguageManagerNew
  		langM <- sourceLanguageManagerGetLanguage lm "haskell"
  		lang <- case langM of
    			(Just lang) -> return lang
    			Nothing -> do
      				langDirs <- sourceLanguageManagerGetSearchPath lm
      				error ("please copy haskell.lang to one of the following directories:\n" ++unlines langDirs)

  			-- create a new SourceBuffer object
  		buffer <- sourceBufferNewWithLanguage lang

  -- load up and display a file
  		fileContents <- readFile "SourceView.hs"
  		textBufferSetText buffer fileContents
  		textBufferSetModified buffer False

  		sourceBufferSetHighlightSyntax buffer True
		

		sourceView <- sourceViewNewWithBuffer buffer
		gutter <- sourceViewGetGutter sourceView TextWindowLeft
 		cell   <- cellRendererTextNew
  		sourceGutterInsert gutter cell 0
		-- Set gutter data.
		sourceGutterSetCellDataFunc gutter cell $ \ c l currentLine -> do
			-- Display line number.
			set (castToCellRendererText c) [cellText := show (l + 1)]
			-- Highlight current line.
			let color = if currentLine 
		                	then Color 65535 0 0 
		                    	else Color 0 65535 0
		 	set (castToCellRendererText c) [cellTextForegroundColor := color]

	  		-- Set gutter size.
  		sourceGutterSetCellSizeFunc gutter cell $ \ c -> 
      		-- -1 mean cell renderer will adjust width with chars dynamically.
      			set (castToCellRendererText c) [cellTextWidthChars := (-1)]

		boxPackStartDefaults (vbox10 gui) sourceView 
		boxReorderChild (vbox10 gui) sourceView   0
		widgetShowAll (vbox10 gui)
		buff3 <- textViewGetBuffer sourceView
		textBufferSetText buff3 (prettyODE tds dpdt )
		labelSetLabel (odelabel gui) "ODE";
	else do	labelSetLabel (odelabel gui) "error";
        

plotClicked :: GUI -> Var [Definition] -> IO ()
plotClicked gui defs =
    do 	tds <- readVar defs;
	if (tds /= []) then do
		pn <- (comboBoxGetActive (plotProComboBox gui))

		if (pn > -1) then do 

			let ps = pros tds;
			let pns = proNames tds
			let p = ps!!(pn);
			plotpointstemp <- entryGetText $ plotPointsEntry gui;
			plotendtemp <- entryGetText $ plotEndEntry gui; 
			plotstarttemp <- entryGetText $ plotStartEntry gui; 			
			--print (show(read plotpointstemp::Int))

			if ((isInteger plotpointstemp ) && (isInteger plotstarttemp) && ( isInteger plotendtemp)) && (((read plotpointstemp::Int) > 1) && ((read plotstarttemp::Int) > -1) && ((read plotendtemp::Int) > 1))
				then do 
					let mts = processMTS tds p
					let dpdt = dPdt tds mts p
					let ts = ((read plotpointstemp),((read plotstarttemp),(read plotendtemp)))
					let ts' = timePoints ts
					let solns = solveODE tds p mts dpdt ts
					let file = "graph.out"
					let ss = speciesIn tds dpdt
					let ss' = speciesInProc p
					let meh = (zip (map pretty ss) (map LA.toList (LA.toColumns solns)))
--					print meh
--					print (meh!!(0))
					--print $ spectablenames meh
					--print $ spectableval meh
					success <- plotTimeSeriesD (drawArea gui) ts' solns ss
					labelSetLabel (plotlabel gui) "Plot";
				else do labelSetLabel (plotlabel gui) "error invalid entries";
		else labelSetLabel (plotlabel gui) "error invalid entries";
	else do	labelSetLabel (plotlabel gui) "error no model loaded";


modelCheckClicked :: GUI -> Var [Definition] -> IO ()
modelCheckClicked gui defs =
	do 	tds <- readVar defs;
		let ps = pros tds;

		let p = ps!!(0);
		buff <- textViewGetBuffer $ modelTextInView gui
		si <- textBufferGetStartIter buff
		ei <- textBufferGetEndIter buff
		text <- textBufferGetText buff si ei False
		let btext = words text
		buff2 <- textViewGetBuffer $ modelTextOutView gui;

		case parseFormula (unwords(btext)) of
                               Left err -> do 
						let text  = "Formula parse error:\n" ++ (show err)
						textBufferSetText buff2 text
                               Right f  -> do 
						let f' = reconcileSpecs tds f
                                          	let text = modelCheckSig tds 
                                                         solveODE 
                                                         Nothing 
                                                         p 
                                      			 (500,(0,2)) 
                                                         f' --}
						--let text = modelCheck tds solveODE Nothing     						           p (10,(0,10)) f
					        textBufferSetText buff2 (show text)



tableClicked :: GUI -> Var [Definition] -> IO ()
tableClicked gui defs =
    do 	
	tds <- readVar defs;
	if (tds /= []) then do
		pn <- (comboBoxGetActive (tabProComboBox gui))

		if (pn > -1) then do 

			let ps = pros tds;
			let pns = proNames tds
			let p = ps!!(pn);
			tabpointstemp <- entryGetText $ tabPointsEntry gui; 	
			tabendtemp <- entryGetText $ tabEndEntry gui; 
			tabstarttemp <- entryGetText $ tabStartEntry gui;		
			if ((isInteger tabpointstemp ) && (isInteger tabstarttemp) && ( isInteger tabendtemp)) && (((read tabpointstemp::Int) > 1) && ((read tabstarttemp::Int) > -1) && ((read tabendtemp::Int) > 1))
				then do 
					
					let mts = processMTS tds p
					let dpdt = dPdt tds mts p
					let ts = ((read tabpointstemp),((read tabstarttemp),(read tabendtemp)))
					let ts' = timePoints ts
					let solns = solveODE tds p mts dpdt ts
					let file = "graph.out"
					let ss = speciesIn tds dpdt
					let ss' = speciesInProc p
					let meh = (zip (map pretty ss) (map LA.toList (LA.toColumns solns)))	
					
					let treeviews = createTrees (length(meh))	
					--treeviewone <- treeviews!!(0)
					{-treeviewtwo <- treeviews!!(1)
					treeviewthree <- treeViewNew
					
					--boxPackStartDefaults (hbox2 gui)  treeviewone
					boxPackStartDefaults (hbox2 gui)  treeviewtwo
					boxPackStartDefaults (hbox2 gui)  treeviewthree

-}
					box <- containerGetChildren (hbox2 gui)
					mapM_ (containerRemove (hbox2 gui)) box

{-					hbox2  <- hBoxNew True 0
					boxPackStartDefaults (vbox6 gui) hbox2 
					boxReorderChild (vbox6 gui) hbox2   0

-}					--fillTable solns spec hbox2 (length spec) (length spec)
					
					--addTableColumn gui (treeView1 gui ) store $ meh!!(0)
					--addTableColumn gui treeviewone store $ meh!!(0)
					--addTableColumn gui treeviewtwo store2 $ meh!!(1)

					--addTableColumnwithoutstore gui treeviewone $ meh!!(0)				
					--addTableColumnwithoutstore gui treeviewtwo $ meh!!(1)
					--addTableColumnwithoutstore gui treeviewthree $ meh!!(2)
					fillTable gui meh treeviews (length(meh))
					--addemptyTableColumn gui $ meh!!(2)					--addTableColumn gui $ meh!!(2)
					--print "blah"
					--addTableColumn solns spec gui hbox2 2

			
					widgetShowAll (hbox2 gui) 
					labelSetLabel (tablabel gui) "Table";
				else do	labelSetLabel (tablabel gui) "error invalid entries";
			else do	labelSetLabel (tablabel gui) "error invalid entries";
	else do	labelSetLabel (tablabel gui) "error no model loaded";
	
csvClicked :: GUI -> Var [Definition] -> IO ()
csvClicked gui defs =
 do 	
	tds <- readVar defs;
	if (tds /= []) then do
		pn <- (comboBoxGetActive (tabProComboBox gui))

		if (pn > -1) then do 

			let ps = pros tds;
			let pns = proNames tds
			let p = ps!!(pn);
			
			tabpointstemp <- entryGetText $ tabPointsEntry gui; 	
			tabendtemp <- entryGetText $ tabEndEntry gui; 
			tabstarttemp <- entryGetText $ tabStartEntry gui;		
			if ((isInteger tabpointstemp ) && (isInteger tabstarttemp) && ( isInteger tabendtemp)) && (((read tabpointstemp::Int) > 1) && ((read tabstarttemp::Int) > -1) && ((read tabendtemp::Int) > 1))
				then do 
					 
					
					let timestep =   (fromInteger $ round $ (((read tabendtemp)-(read tabstarttemp))/((read tabpointstemp)-1)) * (10^2)) / (10.0^^2)
					let mts = processMTS tds p
					let dpdt = dPdt tds mts p
					let ts = ((read tabpointstemp),((read tabstarttemp),(read tabendtemp)))
					let ts' = timePoints ts
					let solns = solveODE tds p mts dpdt ts
					let file = "graph.out"
					let ss = speciesIn tds dpdt
					let ss' = speciesInProc p
					let meh = (zip (map pretty ss) (map LA.toList (LA.toColumns solns)))	
					--mapM_ print meh
					let nms = spectablenames meh
					let vs = spectableval meh
					--print vs
					let vs2 = (transpose vs)
					let vs3 = addTimes vs2 timestep (fromIntegral(read tabstarttemp)) (length(vs2)) (length(vs2))
					let vs4 = map vcsvise vs3
					--putStrLn $ vcsvise (vs2!!(0))

					
					--putStrLn $ (namecsvise nms) ++ (concat vs3)
					let csvoutput = "Timepoint," ++ (namecsvise nms) ++ (concat vs4)
					writeFile "output.csv" csvoutput
					--repeatnumbers 3 3 vs
					labelSetLabel (tablabel gui) "Table";
				else do	labelSetLabel (tablabel gui) "error invalid entries";
			else do	labelSetLabel (tablabel gui) "error invalid entries";
	else do	labelSetLabel (tablabel gui) "error no model loaded";


namecsvise :: [String] -> String
namecsvise [] = []
namecsvise (x:xs) =  if ((length(xs)) > 0) 
			then ((id x) ++ "," ) ++ namecsvise xs
			else (id x) ++ namecsvise xs ++ "\n"

vcsvise :: [Double] -> String
vcsvise [] = []
vcsvise (x:xs) =  if ((length(xs)) > 0) 
			then ((show x) ++ "," ) ++ vcsvise xs
			else (show x) ++ vcsvise xs ++ "\n"


valcsvise ::   Int ->[Double]-> IO()
valcsvise _ [] = return()
valcsvise n xs  = if n > length(xs) 
			then return() 
			else print (xs!!(n))

addTimes :: [[Double]] -> Double -> Double -> Int -> Int -> [[Double]]
addTimes [] _ _ _ _ = []
addTimes (x:xs) ts strt n i = do
				let temp = ((( (fromIntegral(n - i))*ts) + strt) : x) 
				temp : addTimes xs ts strt n (i-1) 
				



repeatnumbers 0 _ vs = return ()
repeatnumbers n i vs =
 do
  mapM_ (valcsvise (i-n)) vs
  repeatnumbers (n-1) i vs



phaseClicked :: GUI -> Var [Definition] -> IO ()
phaseClicked gui defs =
    do 	tds <- readVar defs;
	if (tds /= []) then do
		pn <- (comboBoxGetActive (phaseProComboBox gui))
		s1n <- (comboBoxGetActive (phaseSpecies1Combobox gui))
		s2n <- (comboBoxGetActive (phaseSpecies2Combobox gui))
		if ((pn > -1 )&& (s1n > -1) && (s2n > -1)) then do 

			let ps = pros tds;
			let pns = proNames tds
			let sns = specs tds;
			print $ length(sns)
			let p = ps!!(pn);
			print s1n
			print s2n
			let ss1 = sns!!(s1n)
			let ss2 = sns!!(s2n)
			print "meh"
			phasepointstemp <- entryGetText $ phasePointsEntry gui; 	
			phaseendtemp <- entryGetText $ phaseEndEntry gui; 
			phasestarttemp <- entryGetText $ phaseStartEntry gui;
			if ((read phasepointstemp) > 1 )then do 
				let mts = processMTS tds p
				let dpdt = dPdt tds mts p
				let ts = ((read phasepointstemp),((read phasestarttemp),(read phaseendtemp)))
				let ts' = timePoints ts
				let solns = solveODE tds p mts dpdt ts
				let file = "graph.out"
				let ss = speciesIn tds dpdt
				let ss' = speciesInProc p
				print $ length ss'
				success <- phasePlot2D (drawArea2 gui) ts' solns ss ((ss'!!(s1n)), (ss'!!(s2n))) 
				labelSetLabel (phaselabel gui) "Phase Plot"
			else do	labelSetLabel (phaselabel gui) "error";
		else do	labelSetLabel (tablabel gui) "error invalid entries";
	else do	labelSetLabel (tablabel gui) "error invalid entries";



trueClicked :: GUI -> Var [Definition] -> IO ()
trueClicked gui defs = 
		do tds <- readVar defs;
			if (tds /= []) 
				then do
					entryAppendText (formulaEntry gui) "true "
			else do	labelSetLabel (qBLabel gui) "error";

falseClicked :: GUI -> Var [Definition] -> IO ()
falseClicked gui defs = 
		do tds <- readVar defs;
			if (tds /= []) 
				then do
					entryAppendText (formulaEntry gui) "false "
			else do	labelSetLabel (qBLabel gui) "error";

andDoneClicked :: GUI -> Var [Definition] -> IO ()
andDoneClicked gui defs = 
		do tds <- readVar defs;
			if (tds /= []) 
				then do
				entry1 <- entryGetText $ andEntry1 gui; 
				entry2 <- entryGetText $ andEntry2 gui; 
				let fullEntry =  (entry1 ++ " and " ++ entry2 ++ " ")
				entryAppendText (formulaEntry gui) ( fullEntry)
				widgetHide (andDialog gui)
			else do	labelSetLabel (qBLabel gui) "error";
				widgetHide (andDialog gui)

addFormClicked :: GUI -> Var [Definition] -> IO ()
addFormClicked gui defs = 
		do 
			tds <- readVar defs;
			if (tds /= []) 
				then do
				form <- entryGetText $ formulaEntry gui; 
				buff <- textViewGetBuffer $ textview3 gui;
				textBufferSetText buff (form)
				entrySetText (formulaEntry gui) ( "")
			else do	labelSetLabel (qBLabel gui) "error";

onlineAddClicked :: GUI -> Var [Definition] -> IO ()
onlineAddClicked gui defs = do	
				inf <- openFile ("connections.ocs") ReadMode
                    		inputData <- hGetContents inf
                    		buff <- textViewGetBuffer $ textview6 gui
                    		textBufferSetText buff inputData
                    		hClose inf
				name <- entryGetText $ onlineNameEntry gui; 
				id <- entryGetText $ onlineIDEntry gui; 
				url <- entryGetText $ onlineURLEntry gui; 
				let modelConnection = ( name ++ "|" ++ id ++ "|" ++ url )
				si <- textBufferGetStartIter buff
        			ei <- textBufferGetEndIter buff
       				text <- textBufferGetText buff si ei False 
				textBufferSetText buff (text ++ modelConnection)
				outh <- openFile "connections.ocs" WriteMode
        			si <- textBufferGetStartIter buff
       				ei <- textBufferGetEndIter buff
        			text <- textBufferGetText buff si ei False        
        			hPutStrLn outh text
        			hClose outh




type Var a = MVar a

newVar :: a -> IO (Var a)
newVar = newMVar

modifyVar :: Var a -> (a -> IO (a, b)) -> IO b
modifyVar = modifyMVar

modifyVar_ :: Var a -> (a -> IO a) -> IO ()
modifyVar_ = modifyMVar_

readVar :: Var a -> IO a
readVar = readMVar

type Barrier a = MVar a

newBarrier :: IO (Barrier a)
newBarrier = newEmptyMVar

signalBarrier :: Barrier a -> a -> IO ()
signalBarrier = putMVar

waitBarrier :: Barrier a -> IO a
waitBarrier = readMVar

isInteger s = case reads s :: [(Integer, String)] of
  [(_, "")] -> True
  _         -> False


repeatNTimes 0 _ = return ()
repeatNTimes n action =
 do
  action 
  repeatNTimes (n-1) action

createTrees :: Int -> [ IO TreeView]
createTrees 0 = []
createTrees n = 
	do 
	let tv = treeViewNew 
	tv : createTrees (n-1)



{-

addTableEntry :: GUI -> String -> IO()
addTableEntry gui n = 
		do 
		(trows, tcols) <- tableGetSize (table1 gui)
		tlabel<- labelNew (Just n) --( show (( pros tds)!!(rows-1))))
		tableAttachDefaults (table1 gui) tlabel 0 1 trows (trows+1)
addTableEntryAt :: GUI -> String ->  Int -> IO()
addTableEntryAt gui str n = 
		do 
		(trows, tcols) <- tableGetSize (table1 gui)
		tlabel<- labelNew (Just str) --( show (( pros tds)!!(rows-1))))
		tableAttachDefaults (table1 gui) tlabel 0 1 n (n+1)



addParseprocessColumn ds gui hbox7 x = do
			list <- listStoreNew []
			treeview <- treeViewNewWithModel list
			treeViewSetHeadersVisible treeview True
			col <- treeViewColumnNew
			treeViewColumnSetTitle col ("Process")
			renderer <- cellRendererTextNew
			printStringNTimes x list gui solns (rows solns) (rows solns)
			cellLayoutPackStart col renderer False
			cellLayoutSetAttributes col renderer list
				$ \ind -> [cellText := ind]
			treeViewAppendColumn treeview col
			tree <- treeViewGetSelection treeview
			treeSelectionSetMode tree SelectionBrowse --Multiple
			boxPackStartDefaults hbox2  treeview
			boxReorderChild hbox2 treeview x



addemptyTableColumn :: GUI -> (String,[Double]) -> IO()
addemptyTableColumn gui spectable = do

    --cellLayoutClearAttributes column cell
    store <- listStoreNew []
	
    column <- treeViewColumnNew
    cell <- cellRendererTextNew
    cellLayoutPackStart column cell True
    cellLayoutSetAttributes column cell store $ \row -> [ cellText := row ]
    x <- treeViewAppendColumn ( treeView1 gui) column
    print x
    treeViewColumnSetTitle column "My data"
    treeViewSetModel ( treeView1 gui) store
    cellLayoutClearAttributes column cell
--}

addTableColumn :: GUI -> TreeView -> ListStore String -> (String,[Double]) -> IO()
addTableColumn gui tv store spectable = do
			clear store
			let name = (spectableN spectable)
			let vals = (spectableV spectable)
			let valstr = map show vals
			let tree_view = tv
    			appendor store valstr
	    		column <- treeViewColumnNew
	    		cell <- cellRendererTextNew
	 		cellLayoutPackStart column cell True		
	    		cellLayoutSetAttributes column cell store $ \row -> [ cellText := row ]
	 		x <- treeViewAppendColumn tree_view column
			treeViewColumnSetTitle column name
	    		treeViewSetModel tree_view store
			
{-
			let vals = (spectableV spectable)
			list <- listStoreNew vals
			print $ show list
			treeview <- treeViewNewWithModel list
			treeViewSetHeadersVisible treeview True
			col <- treeViewColumnNew
			let name = (spectableN spectable)
			--let vals = (spectableV spectable)
			treeViewColumnSetTitle col (name)
			renderer <- cellRendererTextNew
			--appendor list vals
			--printStringNTimes x list solns (rows solns) (rows solns)
			cellLayoutPackStart col renderer False
			cellLayoutSetAttributes col renderer list
				$ \ind -> [cellText := (show(ind))]
			treeViewAppendColumn treeview col
			tree <- treeViewGetSelection treeview
			treeSelectionSetMode tree SelectionBrowse --Multiple
-}
			--boxPackStartDefaults (hbox2 gui) tree_view
			--boxReorderChild (hbox2 gui) tree_view 0

addTableColumnwithoutstore :: GUI -> TreeView  -> (String,[Double]) -> IO()
addTableColumnwithoutstore gui tree_view spectable = do
			
			let name = (spectableN spectable)
			let vals = (spectableV spectable)
			let valstr = map show vals
			--tree_view <- tv

			store <- listStoreNew []
						
    			appendor store valstr
			
	    		column <- treeViewColumnNew
			
	    		cell <- cellRendererTextNew
			
	    		cellLayoutPackStart column cell True
			
	    		cellLayoutSetAttributes column cell store $ \row -> [ cellText := row ]
			
	    		x <- treeViewAppendColumn tree_view column
			
			
			treeViewColumnSetTitle column name
			
	    		treeViewSetModel tree_view store






fillTable :: GUI -> [(String,[Double])] -> [IO TreeView] -> Int -> IO()
fillTable gui [] [] _ = return()
fillTable gui [] _  _= return()
fillTable gui _ [] _ = return()
fillTable gui _ _ 0 = return()
fillTable gui spectable tvs n = do

				tv <- (tvs!!(n-1))
				addTableColumnwithoutstore gui (tv) (spectable!!(n-1)) 
				
				boxPackStartDefaults (hbox2 gui)  (tv)
				boxReorderChild (hbox2 gui) tv  (n-1)
				widgetShowAll (hbox2 gui) 
				fillTable gui  spectable tvs (n-1)


addPros cb ps 0 i = return ()
addPros cb ps n i =
		do
		comboBoxAppendText cb ((ps!!(i-n))) --print (show (i-n))
		--print ( show ( (@@>) x ( i-n , y)))
		addPros cb ps (n-1) i


addSpecs cb ss 0 i = return ()
addSpecs cb ss n i =
		do
		comboBoxAppendText cb ((ss!!(i-n))) --print (show (i-n))
		--print ( show ( (@@>) x ( i-n , y)))
		addSpecs cb ss (n-1) i


addTim cb = do
	      comboBoxAppendText cb "F"
	      comboBoxAppendText cb "G"
addVal cb = do
	      comboBoxAppendText cb "Species"
	      comboBoxAppendText cb "Num"

addOp cb = do
	      comboBoxAppendText cb "Relational"
	      comboBoxAppendText cb "Arithimetic"

addRel cb = do
		comboBoxAppendText cb "Greater Than"
		comboBoxAppendText cb "Less Than"
		comboBoxAppendText cb "Equals"

addAri cb = do
		comboBoxAppendText cb "+"
		comboBoxAppendText cb "-"
		comboBoxAppendText cb "x"
		comboBoxAppendText cb "/"


printStringNTimes y ls  x 0 i = return ()
printStringNTimes y ls  x n i =
		do
		listStoreAppend ls ( show ( (@@>) x ( i-n, y )))
		--print (show (i-n))
		--print ( show ( (@@>) x ( i-n , y)))
		printStringNTimes y ls x (n-1) i


appendor ls [] = return()
appendor ls ss = mapM_ (listStoreAppend ls) ss 

clear ls = listStoreClear ls


{-
phaseSpeciesFill :: GUI -> Var [Definition] -> IO ()
phaseSpeciesFill gui defs = do
		tds <- readVar defs;
		if (tds /= []) then do
			pn <- (comboBoxGetActive (phaseProComboBox gui))

			if (pn > -1) then do 
				let ps = pros tds;
				let pns = proNames tds
				let p = ps!!(pn);
				let ss = speciesInProc p
				list <- comboBoxSetModelText (phaseSpecies1Combobox gui)
				addSpecs (phaseSpecies1Combobox gui) ss (length(ss)) (length(ss))
				let ss = speciesInProc p
				list1 <- comboBoxSetModelText (phaseSpecies2Combobox gui)
				addSpecs (phaseSpecies2Combobox gui) ss (length(ss)) (length(ss))
			else do print "meh"
		else do print "meh"


-}



logicClicked :: GUI -> IO()
logicClicked gui = do

			logicTemp <- entryGetText $ logicEntry gui; 	
			--textBufferSetText buff ("blahblah")

			print "meh"

boolClicked :: GUI -> IO()
boolClicked gui = do
			boolTemp <- entryGetText $ boolEntry gui; 
			box <- containerGetChildren (table1 gui)
			mapM_ (containerRemove (table1 gui)) box
			tableResize (table1 gui) 2 1 
			lbl <- labelNew  (Just  boolTemp)
			cBtn <- buttonNewWithLabel "Clear"
 			tableAttachDefaults (table1 gui) (lbl) 0 1 0 1		
 			tableAttachDefaults (table1 gui) (cBtn) 0 1 1 2				
			widgetShowAll (table1 gui)
			--connectBtn2Gui gui cBtn
			widgetShowAll (table1 gui)
			tableTemp <- tableNew 4 2 False
			entryTemp0 <- entryNew 
			entryTemp1 <- entryNew 
			entryTemp2 <- entryNew
			entryTemp3 <- entryNew 
			btnTemp0 <- buttonNewWithLabel "Add"
			btnTemp1 <- buttonNewWithLabel "Add"
			btnTemp2 <- buttonNewWithLabel "Add"
			btnTemp3 <- buttonNewWithLabel "Add"
			tableAttachDefaults (tableTemp) (entryTemp0) 0 1 0 1		
			tableAttachDefaults (tableTemp) (entryTemp1) 0 1 1 2	
			tableAttachDefaults (tableTemp) (entryTemp2) 0 1 2 3		
			tableAttachDefaults (tableTemp) (entryTemp3) 0 1 3 4
			tableAttachDefaults (tableTemp) (btnTemp0) 1 2 0 1
			tableAttachDefaults (tableTemp) (btnTemp1) 1 2 1 2
			tableAttachDefaults (tableTemp) (btnTemp2) 1 2 2 3
			tableAttachDefaults (tableTemp) (btnTemp3) 1 2 3 4		
			boxPackStartDefaults (hbox15 gui) (tableTemp)
			widgetShowAll (hbox15 gui)
			--textBufferSetText buff ("blahblah")
			--textBufferInsertAtCursor buff logicTemp


connectBtn2Gui :: String -> GUI -> Button -> IO (ConnectId Button)
connectBtn2Gui str gui lBtn = do
			lBtn `on` buttonActivated $ lBtnClicked str

lBtnClicked str = openUrlBySystemTool ("http://www.ebi.ac.uk/biomodels-main/"++ str)






